# Workcloud #


### What is this repository for? ###

* Workcube Install from Bitbucket
* 1.0

### How do I get set up? ###

* Coldfusion çalışan bir sunucu veya docker'da Zip dosyasını açın.
* Dosyayı IIS veya Apache Web Server altında bir dizine koyun.
* Workcloud isminde local bir web sitesi tanımlayın. 
* workcloud sitesini host dosyasına tanımlayın ve cf web server configuration tool üzerinden site ile cf bağlantısını kurun.
* http://workcloud/ local sitesine girdiğinizde Workcube Install sayfası gelecektir.
* Sayfadaki formu doldurarak bitbucket üzerinden Workcube dosyalarını alarak kurulum yapabilirsiniz.

### Who do I talk to? ###

* Workcube Core Team