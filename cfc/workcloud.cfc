<cfcomponent>
    <cfscript>
        basePath = replaceNoCase(GetBaseTemplatePath(),"/cfc/workcloud.cfc","");
        public void function executeFromDirectory(required string workingDirectory, required string commandName, required array commandArguments) {
            normalizedArguments = [];
            arrayAppend(normalizedArguments, workingDirectory);
            arrayAppend(normalizedArguments, commandName);

            for (arg in commandArguments) {
                arrayAppend(normalizedArguments, arg);
            }

            var argsString = arrayToList(normalizedArguments, " ");

            cfexecute(
                name = expandPath("../bash/execute_from_directory.sh"),
                arguments = argsString,
                outputFile = "local.successOutput",
                errorFile = "local.errorOutput",
                timeout = "10000"
            );
            
            if ( isDefined("local.errorOutput") and len(local.errorOutput) ) {
                saveContent variable = 'fixes' {
                    writeDump( local.errorOutput );
                }
                fileWrite( "/var/www/workcloud/debug/#commandName#_#commandArguments[1]#.html",fixes );
            }
        }
    </cfscript>
    <cffunction name="restart_server" access="remote" returntype="void">
        <cfargument name="site_path" type="string" required="true">
        <cfargument name="system_platform" type="string" required="true">
        <cfscript>
            if(listfindNoCase('Linux,Unix', Server.OS.Name)){
                this.executeFromDirectory("#arguments.site_path#","chmod",["-R","777","."]);
                /* try {this.executeFromDirectory("#arguments.site_path#","git",["config","--local","core.fileMode","false"]);} catch (exType exName) {} */
                this.executeFromDirectory("#arguments.site_path#","git",["rm","--cached","-r","."]);
                this.executeFromDirectory("#arguments.site_path#","git",["reset","--hard"]);
                try {this.executeFromDirectory("#arguments.site_path#","find",[".","-type","f","-exec","chmod","777","{}","+"]);} catch (exType exName) {}
                if(structKeyExists(server,"lucee") and arguments.system_platform eq 'Server'){
                    this.executeFromDirectory("/","cp",["-r","#basePath#/WEB-INF","#arguments.site_path#"]);
                    
                    cfexecute(
                        name = "/bin/bash",
                        arguments = "-c '/var/www/workcloud/bash/execute_lucee_restart.sh'", 
                        variable = "local.successOutputLucee", 
                        errorVariable = "local.errorOutputLucee", 
                        timeout = 30000
                    );
                    if ( isDefined("local.errorOutputLucee") and len(local.errorOutputLucee) ) {
                        saveContent variable = 'fixes' {
                            writeDump( local.errorOutputLucee );
                        }
                        fileWrite( "/var/www/workcloud/debug/lucee_restart.html",fixes );
                    }
                }
            }
        </cfscript>
    </cffunction>
    <cffunction name="check_server" access="remote" returntype="any" returnformat="JSON">
        <cfreturn {"status": true} />
    </cffunction>
</cfcomponent>