<cfsetting requestTimeOut = "1000">

<cfscript>
	CRLF = Chr(13) & Chr(10);
	attributes = attributes?:structNew();
	StructAppend(attributes, url, "no");
	StructAppend(attributes, form, "no");
	installUrl = cgi.SERVER_PORT eq '80' ? "http://#cgi.HTTP_HOST#" : "https://#cgi.HTTP_HOST#";
    gitUrl = 'https://bitbucket.org/workcube/devcatalyst.git';
    basePath = replaceNoCase(GetBaseTemplatePath(),"index.cfm","");
</cfscript>

<cfif IsDefined("attributes.install")>
    <cfif DirectoryExists(attributes.site_path)>
        <cftry>
            
            <cfhttp url="https://networg.workcube.com/web_services/uhtil854o2018.cfc?method=CHECK_LICENSE_CODE" result="response" method="post" charset="utf-8">
                <cfhttpparam name="api_key" type="formfield" value="20180911HjPo356h">
                <cfhttpparam name="domain_address" type="formfield" value="#attributes.domain_address#">
                <cfhttpparam name="license_code" type="formfield" value="#attributes.license_code#">
            </cfhttp>

            <cfif response.Statuscode contains '200'>
                <cfset responseData = response.filecontent> 
                <cfset cfData = DeserializeJSON(responseData)>
                <cfif cfData.STATUS>

                    <cfhttp url="https://networg.workcube.com/web_services/webserviceforrelease.cfc?method=CHECK_GIT_CODE" result="responseGitCode" method="post" charset="utf-8">
                        <cfhttpparam name="api_key" type="formfield" value="201118kSm20">
                        <cfhttpparam name="release_no" type="formfield" value="#attributes.release_no#">
                        <cfhttpparam name="git_username" type="formfield" value="#attributes.git_username#">
                        <cfhttpparam name="git_password" type="formfield" value="#attributes.git_password#">
                    </cfhttp>
            
                    <cfif responseGitCode.Statuscode contains '200'>
                        <cfset checkGitCode =  DeserializeJSON(Replace(responseGitCode.filecontent,"//",""))>
                        <cfif checkGitCode.status>

                            <cfscript>
                                public void function executeFromDirectory(required string workingDirectory, required string commandName, required array commandArguments) {
                                    normalizedArguments = [];
                                    arrayAppend(normalizedArguments, workingDirectory);
                                    arrayAppend(normalizedArguments, commandName);
                                    for (arg in commandArguments) {
                                        arrayAppend(normalizedArguments, arg);
                                    }

                                    argsString = arrayToList(normalizedArguments, " ");
                                    
                                    cfexecute(
                                        name = expandPath("./bash/execute_from_directory.sh"),
                                        arguments = argsString,
                                        outputFile = "local.successOutput",
                                        errorFile = "local.errorOutput",
                                        timeout = "10000"
                                    );
                                    
                                    if ( isDefined("local.errorOutput") and len(local.errorOutput) ) {
                                        dump( local.errorOutput );
                                        abort;
                                    }
                                }
                            </cfscript>

							<cfset attributes.site_path = replace(attributes.site_path,'\','/','all') />
                            <cfif findNoCase('Windows', Server.OS.Name)>
                                <cfif attributes.web_server_type eq 'IIS'>
                                    <cfset console_app = "C:\Windows\system32\cmd.exe">
                                    <!--- iis web site --->
                                    <cfset app_server_path = "/c C:\Windows\System32\inetsrv\appcmd.exe">
                                    <cfset param = "add site /name:""#attributes.domain_address#"" /bindings:http://#attributes.domain_address#:80 /physicalpath:""#attributes.site_path#"" ">
                                    <cfexecute name="#console_app#" arguments="#app_server_path# #param#" variable="console_result" timeout="60"></cfexecute>
                                    <!--- cf wsconfig --->
                                    
                                    <cfset app_server_path = "/c #Server.ColdFusion.RootDir#\runtime\bin\wsconfig.exe">
                                    <cfset param = "-ws iis -site ""#attributes.domain_address#"" -v ">
                                    <cfexecute name="#console_app#" arguments="#app_server_path# #param#" variable="console_result" timeout="180"></cfexecute>
                                </cfif>
                            <cfelse>
                                <cfif attributes.web_server_type eq 'Apache'>
                                    <cfset site_conf_file = "#replace(attributes.domain_address,".","-","all")#.conf" />
                                    <cfset site_conf_path = "/etc/apache2/sites-available/" />
                                    <cfset site_conf_file_path = "/etc/apache2/sites-available/#site_conf_file#" />
                                    <cfsavecontent variable="web_server_config">
                                    <VirtualHost *:80>
                                        ServerAdmin workcube@workcube.com
                                        DocumentRoot "<cfoutput>#Left(attributes.site_path, len(attributes.site_path)-1)#</cfoutput>"
                                        ServerName <cfoutput>#attributes.domain_address#</cfoutput>
                                        ErrorLog ${APACHE_LOG_DIR}/error.log
                                        CustomLog ${APACHE_LOG_DIR}/access.log combined

                                        <Directory "<cfoutput>#Left(attributes.site_path, len(attributes.site_path)-1)#</cfoutput>">
                                            Options FollowSymLinks
                                            AllowOverride All
                                            Require all granted
                                        </Directory>

                                        DirectoryIndex index.cfm
                                    </VirtualHost>
                                    </cfsavecontent>
                                    <cffile action = "write" file = "#site_conf_file_path#" output = "#web_server_config#" addNewLine = "no" charset = "utf-8" mode = "777">
                                    <cfif attributes.system_platform eq 'Server'>
                                        <cfscript>executeFromDirectory("#site_conf_path#","a2ensite",["#site_conf_file#"]);</cfscript>
                                        <cfscript>executeFromDirectory("#site_conf_path#","systemctl",["reload","apache2"]);</cfscript>
                                    </cfif>
                                <cfelseif attributes.web_server_type eq 'Nginx'>
                                    <cfset site_conf_file = "#replace(attributes.domain_address,".","-","all")#.conf" />
                                    <cfset site_conf_path = "/etc/nginx/sites-available/" />
                                    <cfset site_conf_file_path = "/etc/nginx/sites-available/#site_conf_file#" />
                                    <cfsavecontent variable="web_server_config">
                                    server{
                                        listen 80;
                                        server_name <cfoutput>#attributes.domain_address#</cfoutput>;
                                        root <cfoutput>#Left(attributes.site_path, len(attributes.site_path)-1)#</cfoutput>;
                                        <cfif structKeyExists(server,"lucee")>
                                            set $lucee_context "<cfoutput>#attributes.domain_address#</cfoutput>";
                                            include lucee.conf;
                                        </cfif>
                                    }
                                    </cfsavecontent>
                                    <cffile action = "write" file = "#site_conf_file_path#" output = "#web_server_config#" addNewLine = "no" charset = "utf-8" mode = "777">
                                    <!--- <cfscript>executeFromDirectory("#site_conf_path#","systemctl",["reload","nginx"]);</cfscript> --->
                                    <cfscript>executeFromDirectory("#site_conf_path#","service",["nginx","reload"]);</cfscript>
                                </cfif>
                            </cfif>

                            <cfscript>
                                cfGit = createObject( "component", "cfc/cfGit" );
                                cfGit.init( 
                                    argGit_path : "git", 
                                    argGit_folder : attributes.site_path,
                                    argGit_url : gitUrl,
                                    argGit_username : attributes.git_username,
                                    argGit_password : attributes.git_password
                                );
                                cloneResponse = cfGit.clone_with_branch("releases/#attributes.release_no#");
                                if( cloneResponse.status || (findNoCase( "Cloning into", cloneResponse.result )) ){
                                    //location( url = "http://#attributes.domain_address#/workcloud/?from_workcloud=1&domain_address=#attributes.domain_address#&license_code=#attributes.license_code#&git_username=#attributes.git_username#&git_password=#attributes.git_password#", addToken = "no" );
                                }else{
                                    WriteOutput("There is an error!");
                                    writeDump(cloneResponse);
                                }
                            </cfscript>

                        <cfelse>
                            <script>
                                alert("<cfoutput>#checkGitCode.MESSAGE#</cfoutput>");
                                location.href = "<cfoutput>#installUrl#</cfoutput>";
                            </script>
                        </cfif>
                    <cfelse>
                        <script>
                            alert("Git verification failed");
                            location.href = "<cfoutput>#installUrl#</cfoutput>";
                        </script>
                    </cfif>
                <cfelse>
                    <script>
                        alert("<cfoutput>#cfData.MESSAGE#</cfoutput>");
                        location.href = "<cfoutput>#installUrl#</cfoutput>";
                    </script>
                </cfif>
            <cfelse>
                <script>
                    alert("License verification failed");
                    location.href = "<cfoutput>#installUrl#</cfoutput>";
                </script>
            </cfif>
            <cfcatch type="any">
                <script>
                    alert("There is an error!\nError: <cfoutput>#cfcatch.message#</cfoutput>");
                    location.href = "<cfoutput>#installUrl#</cfoutput>";
                </script>
            </cfcatch>
        </cftry>
    <cfelse>
        <script>
            alert("Site path is not exist!");
            location.href = "<cfoutput>#installUrl#</cfoutput>";
        </script>
    </cfif>
</cfif>

<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="html/fonts/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="html/fonts/gui_custom.css">
	<link rel="stylesheet" href="html/css/bootstrap.min.css">
	<link rel="stylesheet" href="html/css/style.css">
	<script src="html/js/jquery-3.2.1.min.js"></script>
</head>

<cfhttp url="https://networg.workcube.com/web_services/webserviceforrelease.cfc?method=GET_UPGRADE_NOTES" result="response" method="post" charset="utf-8">
    <cfhttpparam name="api_key" type="formfield" value="201118kSm20">
    <cfhttpparam name="session_ep_language" type="formfield" value="tr">
	<cfhttpparam name="get_last_release" type="formfield" value="1">
</cfhttp>

<body>
    <div class="install_panel row">
		<header class="col-md-12 col-xs-12"><h3>Workcube Install</h3></header>
        <cfif IsDefined("attributes.install")>
            <div class="ui-form-list">
                <div class="col-md-12">
                    <h4>Your information has been accepted! Please click to Next Step Button for continue!</h4>
                </div>
            </div>
            <div class="ui-form-list-btn">
                <div class="col-md-12">
                    <div class="form-group button-panel">
                        <input class="btn btn-info" id="restart_server" type="button" onclick="restart_server()" value="Next Step">
                    </div>
                </div>
            </div>
        <cfelse>
            <cfif response.Statuscode contains '200'>
                <cfset responseData =  Replace(response.filecontent,"//","") /> 
                <cfset lastRelease = DeserializeJSON( responseData ) />
                <cfform name="installation_1" id="installation_1" class="formControl" action="#installUrl#" method="post">
                    <input type="hidden" name="install" id="install" value="1" />
                    <div class="ui-form-list">
                        <div class="col-md-12">
                            <div class="col-md-6 col-xs-12 paddingLess">
                                <div class="form-group">
                                    <label>Domain Address <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Domain Address"  name="domain_address" id="domain_address" type="text" value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>System Ip <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Server Local IP"  name="workcube_ip" id="workcube_ip" type="text" value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Workcube Licence Code <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Workcube License Code"  name="license_code" id="license_code" type="text" value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Platform <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <select required class="form-control" message="Enter Platform" name="system_platform" id="system_platform">
                                            <option value="Server">Server</option>
                                            <option value="Docker">Docker</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Web Server Type <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <select required class="form-control" message="Enter Web Server Type" name="web_server_type" id="web_server_type">
                                            <option value="IIS">IIS</option>
                                            <option value="Apache">Apache</option>
                                            <option value="Nginx">Nginx</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Site Path <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Site Path"  name="site_path" id="site_path" type="text" value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Git Username <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Git Username"  name="git_username" id="git_username" type="text" value="<cfoutput>#lastRelease.RELEASE[1].GIT_USERNAME#</cfoutput>" readonly/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Git Password <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl">
                                        <input required class="form-control" message="Enter Git Password"  name="git_password" id="git_password" type="password" value="<cfoutput>#lastRelease.RELEASE[1].GIT_PASSWORD#</cfoutput>" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 paddingLess">
                                <div class="form-group">
                                    <label>Last Release <font color="red">*</font></label>
                                    <div class="col-md-12 pdnl pdnr">
                                        <cfinput required class="form-control" name="release_no" id="release_no" type="text" value="#lastRelease.RELEASE[1].RELEASE#" readonly />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <cfif len(lastRelease.RELEASE[1].MERGE_DATE)>
                                        <label>Last Merge Date <font color="red">*</font></label>
                                    <cfelse>
                                        <label>Last Release Date <font color="red">*</font></label>
                                    </cfif>
                                    <div class="col-md-12 pdnl pdnr">
                                        <cfinput required class="form-control" name="release_date" id="release_date" type="text" value="#dateformat(len(lastRelease.RELEASE[1].MERGE_DATE) ? lastRelease.RELEASE[1].MERGE_DATE : lastRelease.RELEASE[1].NOTE_DATE,'dd/mm/yyyy')#" readonly />
                                    </div>
                                </div>
                                <cfif len(lastRelease.RELEASE[1].PATCH_INFO)>
                                    <cfscript> 
                                        patchInfo = arrayFilter(deserializeJSON( lastRelease.RELEASE[1].PATCH_INFO ), function( el ){ return el.patch_status; });
                                        lastPatch = arrayLen(patchInfo) ? patchInfo : '';
                                    </cfscript>
                                    <cfif isStruct(lastPatch)>
                                        <div class="form-group">
                                            <label>Last Patch <font color="red">*</font></label>
                                            <div class="col-md-12 pdnl pdnr">
                                                <cfinput required class="form-control" name="patch_no" id="patch_no" type="text" value="#lastPatch.PATCH_NO#" readonly />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Last Patch Date<font color="red">*</font></label>
                                            <div class="col-md-12 pdnl pdnr">
                                                <cfinput required class="form-control" name="patch_date" id="patch_date" type="text" value="#lastPatch.PATCH_DATE#" readonly />
                                            </div>
                                        </div>
                                    </cfif>
                                </cfif>
                            </div>
                        </div>
                    </div>
                    <div class="ui-form-list-btn">
                        <div class="col-md-12">
                            <div class="form-group button-panel">
                                <input class="btn btn-info" id="check_license" type="submit" value="Next Step">
                            </div>
                        </div>
                    </div>
                </cfform>
            <cfelse>
                <script>
                    alert('There is an error when getting last release informations from Workcube!\nPlese check your internet connection settings!');
                </script>
            </cfif>
        </cfif>
    </div>
</body>

<script src="html/js/bootstrap.min.js"></script>
<script src="html/js/script.js"></script>
<script>
    $(function(){
        $.install.formControl();
        $.install.formLoading();
    });
    var check_server_counter = 0;
    <cfif IsDefined("attributes.install")>
        function check_server() {
            $.ajax({
                type:'POST',
                dataType: 'json',
                url:'cfc/workcloud.cfc?method=check_server',
                success: function ( response ) {
                    if( response.status ){
                        location.href = "<cfoutput>http://#attributes.domain_address#/workcloud/?from_workcloud=1&domain_address=#attributes.domain_address#&license_code=#attributes.license_code#&workcube_ip=#attributes.workcube_ip#</cfoutput>";
                    };
                },
                error: function ( msg ){console.log(msg);}
            });
        }
        function set_check_server() {
            setInterval(check_server, 10000);
        }
        function restart_server() {
            var restart_server_button = $("input[id = restart_server]");
            restart_server_button.prop("disabled",true).before('<span class="loading"><img src="html/files/images/loader.gif" width="40" height="40"></span>');
            $.ajax({
                type:'POST',
                dataType: 'json',
                url:'cfc/workcloud.cfc?method=restart_server',
                data:{site_path: "<cfoutput>#attributes.site_path#</cfoutput>", system_platform: "<cfoutput>#attributes.system_platform#</cfoutput>"},
                success: function ( response ) {},
                error: function ( msg ){console.log(msg);}
            });
            setTimeout(set_check_server, 120000);
        }
    </cfif>
</script>

</html>